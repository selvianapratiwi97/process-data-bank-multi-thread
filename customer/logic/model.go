package logic

type dataCustomer struct {
	ID               int
	Name             string
	Age              int
	Balanced         int
	ThreadNo2b       string
	ThreadNo3        string
	PreviousBalanced int
	AverageBalanced  int
	ThreadNo1        string
	FreeTransfer     int
	ThreadNo2a       string
}
