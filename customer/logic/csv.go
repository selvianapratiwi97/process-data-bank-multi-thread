package logic

import (
	"encoding/csv"
	"os"
	"fmt"
	"strconv"
)

func getDataCsvFile() [][]string {
	csvFile, err := os.Open("Before-Eod.csv")
	if err != nil {
		fmt.Println("Failed open csv, error : ", err)
	}
	fmt.Println("Successfully Opened CSV file")
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println("Failed read csv, error : ", err)
	}

	return csvLines
}

func writeDataCustomerToCSV(customers []dataCustomer) {
	file, err := os.Create("After-Eod.csv")
	defer file.Close()
	if err != nil {
		fmt.Println("failed to create file : ", err)
	}

	w := csv.NewWriter(file)
	defer w.Flush()

	header := []string{"id", "Nama", "Age", "Balanced", "No 2b Thread-No", "No 3 Thread-No",
		"Previous Balanced", "Average Balanced", "No 1 Thread-No", "Free Transfer", "No 2a Thread-No"}
	if err := w.Write(header); err != nil {
		fmt.Println("failed to write header : ", err)
	}
	var data [][]string
	for _, record := range customers {
		row := []string{strconv.Itoa(record.ID), record.Name, strconv.Itoa(record.Age), strconv.Itoa(record.Balanced),
			record.ThreadNo2b, record.ThreadNo3,
			strconv.Itoa(record.PreviousBalanced), strconv.Itoa(record.AverageBalanced),
			record.ThreadNo1, strconv.Itoa(record.FreeTransfer), record.ThreadNo2a}
		data = append(data, row)
	}
	if err := w.WriteAll(data); err != nil {
		fmt.Println("failed to write all data : ", err)
	}
}
