package logic

import (
	"strconv"
	"strings"
	"sync"
)

func ProcessDataCustomer() {
	dataCustomer := getAllDataCustomer()

	process_calculateAverageBalanced(dataCustomer)

	process_addFreeTransferToCustomer(dataCustomer)

	process_addBalanceToCustomer(dataCustomer)

	process_addBalanceToCustomer_2(dataCustomer)

	writeDataCustomerToCSV(dataCustomer)
}

func getAllDataCustomer() []dataCustomer {
	csvLines := getDataCsvFile()

	var customerList []dataCustomer
	for i, line := range csvLines {
		if i > 0 {
			var data dataCustomer
			for j, field := range strings.Split(line[0], ";") {
				if j == 0 {
					var err error
					data.ID, err = strconv.Atoi(field)
					if err != nil {
						continue
					}
				} else if j == 1 {
					data.Name = field
				} else if j == 2 {
					var err error
					data.Age, err = strconv.Atoi(field)
					if err != nil {
						continue
					}
				} else if j == 3 {
					var err error
					data.Balanced, err = strconv.Atoi(field)
					if err != nil {
						continue
					}
				} else if j == 4 {
					var err error
					data.PreviousBalanced, err = strconv.Atoi(field)
					if err != nil {
						continue
					}
				} else if j == 5 {
					var err error
					data.AverageBalanced, err = strconv.Atoi(field)
					if err != nil {
						continue
					}
				} else if j == 6 {
					var err error
					data.FreeTransfer, err = strconv.Atoi(field)
					if err != nil {
						continue
					}
				}
			}
			customerList = append(customerList, data)
		}
	}
	return customerList
}

func process_calculateAverageBalanced(dtCustomer []dataCustomer) {
	var wg sync.WaitGroup
	wg.Add(len(dtCustomer))

	noThread := 0
	for i := 0; i < len(dtCustomer); i++ {
		go func(i int) {
			defer wg.Done()

			dtCustomer[i].AverageBalanced = (dtCustomer[i].Balanced + dtCustomer[i].PreviousBalanced) / 2
			dtCustomer[i].ThreadNo1 = "No-Thread-" + strconv.Itoa(noThread)
			noThread++
		}(i)
	}
	wg.Wait()
}

func process_addFreeTransferToCustomer(dtCustomer []dataCustomer) {
	var wg2 sync.WaitGroup
	wg2.Add(len(dtCustomer))

	noThread2a := 0
	for i := 0; i < len(dtCustomer); i++ {
		go func(i int) {
			defer wg2.Done()

			if dtCustomer[i].Balanced >= 100 && dtCustomer[i].Balanced <= 150 {
				dtCustomer[i].FreeTransfer = 5
			}

			dtCustomer[i].ThreadNo2a = "No-Thread-" + strconv.Itoa(noThread2a)
			noThread2a++
		}(i)
	}
	wg2.Wait()
}

func process_addBalanceToCustomer(dtCustomer []dataCustomer) {
	var wg3 sync.WaitGroup
	wg3.Add(len(dtCustomer))

	noThread2b := 0
	for j := 0; j < len(dtCustomer); j++ {
		go func(j int) {
			defer wg3.Done()

			if dtCustomer[j].Balanced > 150 {
				dtCustomer[j].Balanced = dtCustomer[j].Balanced + 25
			}

			dtCustomer[j].ThreadNo2b = "No-Thread-" + strconv.Itoa(noThread2b)
			noThread2b++
		}(j)
	}
	wg3.Wait()
}

func process_addBalanceToCustomer_2(dtCustomer []dataCustomer) {
	var result [][]dataCustomer

	for i := 0; i < 8; i++ {
		min := (i * len(dtCustomer) / 8)
		max := ((i + 1) * len(dtCustomer)) / 8
		result = append(result, dtCustomer[min:max])
	}

	var wg4 sync.WaitGroup
	wg4.Add(len(result))

	noThread3 := 0
	for i := 0; i < len(result); i++ {
		go func(i int) {
			defer wg4.Done()
			data := result[i]

			for n := 0; n < len(data); n++ {
				if data[n].ID <= 100 {
					data[n].Balanced = data[n].Balanced + 10
				}
				data[n].ThreadNo3 = "No-Thread-" + strconv.Itoa(noThread3)
			}
			noThread3++
		}(i)
	}
	wg4.Wait()
}
